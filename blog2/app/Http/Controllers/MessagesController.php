<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Message;
use App\Http\Requests\CreateMessageRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Middleware;


class MessagesController extends Controller
{
      function __construct(){
      $this->middleware('verificar',['except' => ['create','store']]);
    }

    public function index()
    {
        //$messages = ::table('messages')->get();

        $messages = Message::all();

        return view('messages.index',compact('messages'));
    }

    public function create()
    {
        return view('messages.create');
    }

    public function store(CreateMessageRequest $request)
    {
        /*DB::table('messages')->insert([
            "nombre" => $request->input('nombre'),
            "email" => $request->input('email'),
            "mensaje" => $request->input('mensaje'),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ]); */
        Message::create($request->all());

        return redirect()->route('mensajes.create')->with('info', 'Hemos recibido tu mensaje');
    }

    public function show($id)
    {
        //$message = DB::table('messages')->where('id',$id)->first();
        $message = Message::findOrFail($id);

        return view('messages.show',compact('message'));
    }


    public function edit($id)
    {
        //$message = DB::table('messages')->where('id',$id)->first();

        $message = Message::findOrFail($id);

        return view('messages.edit', compact('message'));
    }


    public function update(CreateMessageRequest $request, $id)
    {
        /*DB::table('messages')->where('id',$id)->update([
            "nombre" => $request->input('nombre'),
            "email" => $request->input('email'),
            "mensaje" => $request->input('mensaje'),
            "updated_at" => Carbon::now(),
        ]);*/

        $message = Message::findOrFail($id)->update($request->all());

        return redirect()->route('mensajes.index');
    }

    public function destroy($id)
    {
        //DB::table('messages')->where('id',$id)->delete();
        $message = Message::findOrFail($id)->delete();

        return redirect()->route('mensajes.index');
    }
}
