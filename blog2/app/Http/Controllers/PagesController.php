<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Middleware;

class PagesController extends Controller
{


    public function __construct(){
        $this->middleware('example',['except'=> ['home']]);
    }

    public function home(){
        return view('home');
    }


    public function mensaje(CreateMessageRequest $request){
        $data = $request->all();

        return back()->with('info','Tu mensaje ha sido enviado correctamente :D');
    }

    public function saludo($nombre = "invitado"){
        $html = "<h2>Contenido HTML</h2>";
        $script = "<script>alert('Problema XSS - Cross site Scripting!')</script>";
        $consolas = [
            "Play Station 4",
            "Xbox One",
            "Wii U"
        ];
        return view('saludo', compact('nombre','html','script','consolas'));
    }
}
